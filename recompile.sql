SET SERVEROUTPUT ON SIZE 1000000
BEGIN
FOR cur_rec IN (
 SELECT   DECODE( object_type, 'PACKAGE BODY',
            'alter package ' || owner||'.'||object_name || ' compile body;',
            'alter ' || object_type || ' ' || owner||'.'||object_name || ' compile;' ) sentence,
          DECODE(object_type, 'PACKAGE', 1, 2, 2) AS recompile_order,
          object_type,
          owner,
          object_name
 FROM     dba_objects 
 WHERE    STATUS = 'INVALID'
 AND      object_type in ( 'PACKAGE BODY', 'PACKAGE', 'FUNCTION', 'PROCEDURE', 'TRIGGER', 'VIEW' )
 ORDER BY 2
)
LOOP
 BEGIN
 EXECUTE IMMEDIATE cur_rec.sentence;
 EXCEPTION
 WHEN OTHERS THEN
  DBMS_OUTPUT.put_line('ERROR, CANT COMPILE ' || cur_rec.object_type || ' : ' || cur_rec.owner ||  
  ' : ' || cur_rec.object_name);
 END;
END LOOP;
END;
/